use std::fmt::{Display, Formatter, Result};
use std::io::stdin;
use std::ops::Deref;

enum Temperature {
    Celsius(f64),
    Fahrenheit(f64),
}

impl Temperature {
    fn from(value: f64, symbol: &str) -> Option<Temperature> {
        match symbol {
            "c" | "C" => Some(Temperature::Celsius(value)),
            "f" | "F" => Some(Temperature::Fahrenheit(value)),
            _ => None,
        }
    }

    fn convert(&self) -> Temperature {
        match self {
            Temperature::Celsius(t) => Temperature::Fahrenheit(ctof(*t)),
            Temperature::Fahrenheit(t) => Temperature::Celsius(ftoc(*t)),
        }
    }

    fn symbol(&self) -> &str {
        match self {
            Temperature::Celsius(_) => "C",
            Temperature::Fahrenheit(_) => "F",
        }
    }

    fn value(&self) -> f64 {
        match self {
            Temperature::Celsius(t) => *t,
            Temperature::Fahrenheit(t) => *t,
        }
    }
}

impl Display for Temperature {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{:.2}°{}", self.value(), self.symbol())
    }
}

fn main() {
    println!("Temperature Converter!");

    let temp_value = get_temperature();
    let temperature = get_temperature_unit(temp_value);

    println!("{} is same as {}", temperature, temperature.convert());
}

fn get_input(msg: &str) -> String {
    let mut user_input = String::new();

    println!("{}", msg);

    stdin()
        .read_line(&mut user_input)
        .expect("Failed to read user input");

    // Trimming `user_input` to remove line break captured by `read_line`
    // Note: `"".to_string()` have a higher performance cost than
    // `String::from("")`
    // https://github.com/rust-lang/rust/issues/18404
    String::from(user_input.trim())
}

fn get_temperature() -> f64 {
    let input = get_input("What is the temperature you want to convert? ");

    match input.parse() {
        Ok(val) => val,
        Err(_) => {
            println!("Assuming temperature as 0");

            0.0
        }
    }
}

fn get_temperature_unit(value: f64) -> Temperature {
    loop {
        let input = get_input("Is this temperature Centigrade or Fahrenheit? (C/F)");

        // Check if `Temperature` enum accepts the `input`
        if let Some(temperature) = Temperature::from(value, input.deref()) {
            return temperature;
        }
    }
}

// Convert Celsius to Fahrenheit
// See: https://en.wikipedia.org/wiki/Conversion_of_units_of_temperature
fn ctof(temperature: f64) -> f64 {
    temperature * 9.0 / 5.0 + 32.0
}

// Convert Fahrenheit to Celsius
// See: https://en.wikipedia.org/wiki/Conversion_of_units_of_temperature
fn ftoc(temperature: f64) -> f64 {
    (temperature - 32.0) * 5.0 / 9.0
}
