const Y: u8 = 100;

fn main() {
    // Mutability {{{

    // let x = 5;  // Immutable `x` will cause an error in `x = 6;` line
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);

    // Mutability }}}

    // Constants {{{

    const X: i32 = -123;

    println!("X = {}, Y = {}", X, Y);

    // Constants }}}

    // Shadowing {{{

    let shadow_me = 5;

    let shadow_me = shadow_me + 1;

    let shadow_me = shadow_me * 2;

    println!("Previous `shadow_me` values are shadowed by {}", shadow_me);

    // Following 2 lines will cause error because the on first line, the type
    // `space` is inferred as `&str` and on second it is `usize`
    //
    // let mut spaces = "     ";
    // spaces = spaces.len();

    // In here however it is allowed as we are using new let binding (declaring
    // new `space`) shadowing the previous one, and it can have a different
    // type.
    let spaces = "     ";
    let spaces = spaces.len();

    println!("We have {} spaces", spaces);

    // Shadowing }}}
}
