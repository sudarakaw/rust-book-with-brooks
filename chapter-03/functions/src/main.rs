fn main() {
    // Call another_function_v1 {{{
    println!("Hello, world!");

    another_function_v1();
    // }}}

    // Call another_function_v2 with argument {{{
    let num = 5.0;
    another_function_v2(num);
    // }}}

    // Call another_function_v3 with tuple as an argument {{{
    let pos = (35, 42);
    another_function_v3(pos);
    // }}}

    // Call another_function_v4 with tuple as an argument & returns a value {{{
    let pos = (35, 42);
    let result = another_function_v4(pos);

    println!("Result = {}", result);
    // }}}
}

// another_function_v1 {{{
fn another_function_v1() {
    println!("Another function.");
}
// }}}

// another_function_v2 {{{
fn another_function_v2(x: f64) {
    println!("The value of x is: {}", x);
}
// }}}

// another_function_v3 {{{
fn another_function_v3(pos: (u32, u32)) {
    let (x, y) = pos;

    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
}
// }}}

// another_function_v4 {{{
fn another_function_v4(pos: (u32, u32)) -> u32 {
    let (x, y) = pos;

    println!("The value of x in the function is: {}", x);
    println!("The value of y in the function is: {}", y);

    (x * 2) - y
}
// }}}
