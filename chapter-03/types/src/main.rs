fn main() {
    // Data Types {{{
    let input = "42";

    // Following will cause an error failing to infer type
    // let guess = input.parse().expect("Not a number!");

    // This works
    let guess: u32 = input.parse().expect("Not a number!");

    println!("Guess is {}", guess);
    // }}}

    // Integer Types {{{
    let input = "4299999999999999999";

    // Following will cause a integer overflow runtime exception
    // let guess: u8 = input.parse().expect("Not a number!");

    // Following will work
    // let guess: usize = input.parse().expect("Not a number!");
    let guess: u64 = input.parse().expect("Not a number!");

    println!("Guess is {}", guess);
    // }}}

    // Floating-Point Types {{{
    let x = 2.0; // f64
    let y: f32 = 3.0; // f32

    println!("{}, {}", x, y);

    let a = 0.1;
    let b = 0.2;

    println!("{}", a + b);
    // }}}

    // Character Type {{{
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';

    println!("{}, {}, {}", c, z, heart_eyed_cat);

    // }}}

    // Tuple Type {{{
    let tup = (1, 'a', "hello");

    let (num, ch, greet) = tup;
    let t_ch = tup.1;

    println!("{}, {}, {}", greet, ch, num);
    println!("{}", t_ch);
    // }}}

    // Array Type {{{
    let arr = [1, 2, 3];

    // Following will cause an index out of bound runtime error
    // println!("{}", arr[5]);

    println!("{}", arr[1]);

    // }}}
}
