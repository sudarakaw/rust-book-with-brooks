fn main() {
    let daily_gifts = [
        ("first", "A Partridge in a Pear Tree"),
        ("second", "Two Turtle Doves, and"),
        ("third", "Three French Hens"),
        ("fourth", "Four Calling Birds"),
        ("fifth", "Five Golden Rings"),
        ("sixth", "Six Geese a Laying"),
        ("seventh", "Seven Swans a Swimming"),
        ("eighth", "Eight Maids a Milking"),
        ("ninth", "Nine Ladies Dancing"),
        ("tenth", "Ten Lords a Leaping"),
        ("eleventh", "Eleven Pipers Piping"),
        ("twelfth", "12 Drummers Drumming"),
    ];

    for (idx, day) in daily_gifts.iter().enumerate() {
        println!("On the {} day of Christmas my true love sent to me:", day.0);
        print_gifts(&daily_gifts[0..idx + 1]);
        println!();
    }
}

fn print_gifts(gifts: &[(&str, &str)]) {
    for gift in gifts.iter().rev() {
        println!("{}", gift.1);
    }
}
