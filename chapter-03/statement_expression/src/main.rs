fn main() {
    let x = 5;

    let y = {
        let x = 6;

        x + 1
        // NOTE: can NOT do `return x + 1;` here because we are not returning a
        // value  from a function.
        // Also so see functions/main.rs
    };

    println!("x = {}, y = {}", x, y);
}
