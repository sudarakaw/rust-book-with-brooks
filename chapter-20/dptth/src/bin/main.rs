extern crate dptth;

use dptth::ThreadPool;
use std::fs;
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use std::thread;
use std::time::Duration;

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 512];
    let get_index = b"GET / HTTP/1.1\r\n";
    let get_sleep = b"GET /sleep HTTP/1.1\r\n";

    stream.read(&mut buffer).unwrap();

    // println!("Request:\n{}", String::from_utf8_lossy(&buffer[..]));

    let (status, filename) = if buffer.starts_with(get_index) {
        ("200 OK", "index.html")
    } else if buffer.starts_with(get_sleep) {
        thread::sleep(Duration::from_secs(10));

        ("200 OK", "sleep.html")
    } else {
        ("404 NOT FOUND", "404.html")
    };

    let contents = fs::read_to_string(filename).unwrap();
    let response = format!("HTTP/1.1 {}\r\n\r\n{}", status, contents);

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}

fn main() {
    let listner = TcpListener::bind("127.0.0.1:5000").unwrap();
    let pool = ThreadPool::new(4);

    // for stream in listner.incoming().take(2) {
    for stream in listner.incoming() {
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        });
    }

    println!("Shutting down.");
}
