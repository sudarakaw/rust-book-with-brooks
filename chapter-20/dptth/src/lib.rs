use std::sync::{mpsc, Arc, Mutex};
use std::thread;

type ThreadSafeJob = Arc<Mutex<mpsc::Receiver<Message>>>;
type Job = Box<dyn FnBox + Send + 'static>;

enum Message {
    Start(Job),
    Terminate,
}

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: ThreadSafeJob) -> Worker {
        let thread = thread::spawn(move || loop {
            let msg = receiver.lock().unwrap().recv().unwrap();

            match msg {
                Message::Start(job) => {
                    println!("Worker {} got a job; executing...", id);

                    // NOTE: Rust does not allow the following call at present, it might
                    // in the future.
                    // For now we use the `FnBox` solution here.
                    //
                    // (*job)();

                    job.call_box();
                }
                Message::Terminate => {
                    println!("Worker {} was told to terminate.", id);

                    break;
                }
            }
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

impl ThreadPool {
    /// Create a new ThreadPool
    ///
    /// The `max` is the number of workers (threads) in the pool.
    ///
    /// ##### Panics
    ///
    /// The `ThreadPool::new` function will panic if `max` is zero.
    pub fn new(max: usize) -> ThreadPool {
        assert!(0 < max);

        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers = Vec::with_capacity(max);

        for id in 0..max {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool { workers, sender }
    }

    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(Message::Start(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending terminate message to all workers.");

        // First send `Terminate` message to *all* the workers to make sure were
        // told to terminate before we join them. (messages are delivered to
        // workers in round-robin fashion)
        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Shutting down all workers.");

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}
