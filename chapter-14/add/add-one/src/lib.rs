extern crate rand;

/// Add one to the given `i32` value and return the result.
///
/// ## Example
///
/// ```
/// let value = 41;
///
/// assert_eq!(42, add_one::add_one(value));
///
/// let value = -5;
///
/// assert_eq!(-4, add_one::add_one(value));
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}
