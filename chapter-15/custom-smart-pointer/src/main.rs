use std::fmt::Display;
use std::ops::Deref;

struct XBox<T>(T)
where
    T: Display;

impl<T> XBox<T>
where
    T: Display,
{
    fn new(value: T) -> XBox<T> {
        XBox(value)
    }
}

impl<T> Deref for XBox<T>
where
    T: Display,
{
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> Drop for XBox<T>
where
    T: Display,
{
    fn drop(&mut self) {
        println!("Dropping XBox with data {}", self.0);
    }
}

fn hello(name: &str) {
    println!("Hello {}", name);
}

fn main() {
    let x = 5;
    let y = XBox::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y);

    let sb = XBox::new("TEST!!!!");

    println!("Int box = {}", *y);
    println!("String box = {}", *sb);

    hello("world");
    hello(&sb); // Rust's automatic deref-coercion
    hello(&(*sb)[..]); // Without automatic deref-coercion
}
