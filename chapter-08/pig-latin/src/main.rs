// Convert strings to pig latin. The first consonant of each word is moved to
// the end of the word and “ay” is added, so “first” becomes “irst-fay.” Words
// that start with a vowel have “hay” added to the end instead (“apple” becomes
// “apple-hay”). Keep in mind the details about UTF-8 encoding!

fn is_vowel(letter: &str) -> bool {
    let vowels = "aeiou".chars().collect::<String>();

    if 1 != letter.len() {
        return false;
    }

    vowels.contains(letter)
}

fn pig_latin(str: &str) -> String {
    str.split_whitespace()
        .map(|word| word.chars())
        .map(|letters| {
            (
                letters.clone().take(1).collect::<String>().to_lowercase(),
                letters.clone().skip(1).collect::<String>().to_lowercase(),
            )
        }).map(|(first, rest)| {
            if is_vowel(&first) {
                return format!("{}{}-hay", first, rest);
            }

            format!("{}-{}ay", rest, first)
        }).collect::<Vec<_>>()
        .join(" ")
}

fn main() {
    let sentence = "Hello world";

    println!("{}", pig_latin(sentence));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_vowel_empty() {
        assert_eq!(is_vowel(""), false);
    }

    #[test]
    fn is_vowel_single_letter() {
        assert_eq!(is_vowel("a"), true);
        assert_eq!(is_vowel("b"), false);
        assert_eq!(is_vowel("e"), true);
        assert_eq!(is_vowel("f"), false);
        assert_eq!(is_vowel("i"), true);
        assert_eq!(is_vowel("o"), true);
        assert_eq!(is_vowel("u"), true);
        assert_eq!(is_vowel(" "), false);
        assert_eq!(is_vowel("?"), false);
        assert_eq!(is_vowel("!"), false);
    }

    #[test]
    fn is_vowel_multiple_letters() {
        assert_eq!(is_vowel("aa"), false);
        assert_eq!(is_vowel("xx"), false);
        assert_eq!(is_vowel("ax"), false);
        assert_eq!(is_vowel("xa"), false);
        assert_eq!(is_vowel("  "), false);
    }

    #[test]
    fn test_pig_latin() {
        assert_eq!(pig_latin("Hello world"), "ello-hay orld-way");
        assert_eq!(pig_latin("PIG Latin"), "ig-pay atin-lay");
        assert_eq!(
            pig_latin("Rust programming is exciting"),
            "ust-ray rogramming-pay is-hay exciting-hay"
        );
    }
}
