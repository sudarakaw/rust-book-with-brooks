fn main() {
    let v1: Vec<i32> = Vec::new();
    // Following will not work as vector definition requires type annotation to
    // know what type of items goes in the vector.
    // let v1 = Vec::new();

    // Following works because Rust can determine the item type using the first
    // element given
    let v2 = vec![1, 2, 3, 4, 5];

    // Again, the following does not work because there are no values to
    // determine the vector item type.
    // let v2 = vec![];

    // And it work with type annotation
    let v3: Vec<u8> = vec![];

    // We can create empty vector without type annotation as long as it is
    // mutable & we push a value to it later.
    // Type of the vector items will be determined at the first `push`.
    let mut v4 = Vec::new();

    v4.push(42);

    let value1 = String::from("hello");
    let v5 = vec![value1];

    // Following will not work because vector take ownership of the items we put
    // in it. In other word they are moved in to the vector.
    // println!("{}", value1);

    let value2 = String::from("world");
    let v6 = vec![&value2];

    // This work because we only let vector borrow `value2` by passing reference
    // to it.
    println!("{}", value2);

    // Following fail due to index out-of-bound error...
    // let nx_value1 = &v2[100];
    // println!("{}", nx_value1);

    // But this will not
    let nx_value2 = v2.get(100);
    println!("{:?}", nx_value2);

    // NOTE: `value3` is defined in this (outer) scope.
    let value3 = String::from("TEST");
    let copy_of_value3;

    {
        let mut v7 = Vec::new();

        // We can't give ownership of `value3` to the vector because we use it
        // beyond the lifetime of `v7`.
        // v7.push(value3);

        v7.push(&value3);

        // Can't borrow (get reference) here because `v7` go out-of-scope before
        // we are done with `copy_of_value3`.
        // copy_of_value3 = &v7[0];

        copy_of_value3 = v7[0];
    }

    println!("{}", copy_of_value3);

    // Iteration borrows an immutable reference to the vector item
    for i in &v2 {
        println!("{}", i);
    }

    let mut v8 = vec![100, 32, 57];

    // In this case we borrow a mutable reference to the vector item (and modify
    // the value in the vector via it).
    for i in &mut v8 {
        println!("{}", i);

        *i += 50;
    }

    // Use everything to silent warnings
    println!("{:?}", v1);
    println!("{:?}", v2);
    println!("{:?}", v3);
    println!("{:?}", v4);
    println!("{:?}", v5);
    println!("{:?}", v6);
    println!("{:?}", v8);
}
