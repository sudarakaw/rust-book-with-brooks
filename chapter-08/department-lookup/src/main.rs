// Using a hash map and vectors, create a text interface to allow a user to add
// employee names to a department in a company. For example, “Add Sally to
// Engineering” or “Add Amir to Sales.” Then let the user retrieve a list of all
// people in a department or all people in the company by department, sorted
// alphabetically.

// Commands:
//  Add <name> to <department>
//  Get employees
//  Get employees from <department>
//  Exit

use std::collections::HashMap;
use std::io::{self, Write};

#[derive(Debug)]
enum Command {
    NoOp,
    Add { name: String, department: String },
    Get(Option<String>),
    Exit,
}

fn parse_input(input: &str) -> Command {
    let blocks: Vec<_> = input.trim().split_whitespace().collect();

    let result = match blocks.as_slice() {
        ["exit"] => Command::Exit,
        ["add", name, "to", department] => Command::Add {
            name: name.to_string(),
            department: department.to_string(),
        },
        ["get"] => Command::Get(None),
        ["get", department] => Command::Get(Some(department.to_string())),
        [] => Command::NoOp,
        cmd => {
            println!("Unknown command {}", cmd[0]);

            Command::NoOp
        }
    };

    result
}

fn main() {
    let mut company: HashMap<String, Vec<String>> = HashMap::new();

    loop {
        print!("> ");
        io::stdout().flush().unwrap();

        let mut raw_input = String::new();

        io::stdin()
            .read_line(&mut raw_input)
            .expect("Failed to read line");

        let cmd = parse_input(&raw_input);

        match cmd {
            Command::Exit => break,
            Command::Add { name, department } => {
                match company.get_mut(&department) {
                    None => {
                        company.insert(department, vec![name]);
                    }
                    Some(employees) => {
                        employees.push(name);
                    }
                };
            }
            Command::Get(Some(department)) => match company.get(&department) {
                None => println!("Department {} does not exists.", department),
                Some(employees) => match employees.as_slice() {
                    [] => println!("No employees in department {}.", department),
                    names => {
                        println!("Employees in {} department.", department);
                        println!("{}", names.join("\n"));
                    }
                },
            },
            Command::Get(None) => {
                let names: Vec<_> = company.values().flatten().map(|s| s.to_string()).collect();

                println!("All employees in the company.");

                if 1 > names.len() {
                    println!("No employees found");
                } else {
                    println!("{}", names.join("\n"));
                }
            }
            Command::NoOp => continue,
        };
    }
}
