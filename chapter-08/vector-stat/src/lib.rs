use std::collections::HashMap;

#[derive(Debug)]
pub struct ListStatistics(Vec<i32>);

impl ListStatistics {
    pub fn mean(&self) -> f64 {
        let total: i32 = self.0.iter().sum();

        total as f64 / self.0.len() as f64
    }

    pub fn median(&self) -> f64 {
        let mut ordered_list = self.0.to_vec();
        let middle = self.0.len() / 2;

        ordered_list.sort();

        match middle % 2 {
            0 => ordered_list[middle] as f64,
            _ => ListStatistics(ordered_list[middle - 1..middle + 1].to_vec()).mean(),
        }
    }

    pub fn mode(&self) -> Vec<i32> {
        let mut freq: HashMap<i32, i32> = HashMap::new();
        let mut mode = vec![];

        self.0.iter().for_each(|num| {
            freq.entry(*num)
                .and_modify(|count| *count += 1)
                .or_insert(0);
        });

        let max_count = freq.values().max().unwrap_or(&0);

        if 1 < *max_count {
            freq.iter().for_each(|(num, count)| {
                if count == max_count {
                    mode.push(*num);
                }
            });
        }

        // Sort the result so it's testable using `assert_eq`
        mode.sort();

        mode
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mean() {
        assert_eq!(ListStatistics(vec![1, 2, 3, 4, 5]).mean(), 3.0);
        assert_eq!(ListStatistics(vec![1, 2, 3, 4, 5, 6]).mean(), 3.5);
        assert_eq!(
            ListStatistics(vec![1, 2, 3, 4, 5, 6, 8, 9, 9]).mean(),
            5.222222222222222
        );
    }

    #[test]
    fn median() {
        assert_eq!(ListStatistics(vec![4, 1, 5, 3, 2]).median(), 3.0);
        assert_eq!(ListStatistics(vec![4, 6, 1, 5, 3, 2]).median(), 3.5);
        assert_eq!(
            ListStatistics(vec![4, 6, 1, 8, 9, 9, 3, 2, 4, 6, 7, 8, 1, 3, 6, 2]).median(),
            6.0
        );
    }

    #[test]
    fn mode() {
        assert_eq!(ListStatistics(vec![]).mode(), []);
        assert_eq!(
            ListStatistics(vec![1, 6, 2, 6, 3, 6, 4, 6, 5, 6, 6, 6, 7, 6, 8, 6, 9]).mode(),
            [6]
        );
        assert_eq!(
            ListStatistics(vec![13, 18, 13, 14, 13, 16, 14, 21, 13]).mode(),
            [13]
        );
        assert_eq!(ListStatistics(vec![1, 2, 4, 7]).mode(), []);
        assert_eq!(
            ListStatistics(vec![8, 9, 10, 10, 10, 11, 11, 11, 12, 13]).mode(),
            [10, 11]
        );
        assert_eq!(ListStatistics(vec![6, 6, 6]).mode(), [6]);
    }
}
