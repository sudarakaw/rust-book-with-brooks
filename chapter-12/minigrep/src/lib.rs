#![feature(slice_patterns)]

use std::env;
use std::error::Error;
use std::fs;

#[derive(PartialEq, Debug)]
pub struct Config<'a> {
    pub query: &'a str,
    pub filename: &'a str,
    pub case_sensitive: bool,
}

impl<'a> Config<'a> {
    pub fn parse(args: &[String]) -> Result<Config, String> {
        let case_sensitive = env::var("CASE_SENSITIVE").is_ok();

        match args {
            [_, query, filename, ..] => Ok(Config {
                query,
                filename,
                case_sensitive,
            }),
            [] => Err(String::from("Argument list is empty")),
            _ => Err(format!("Expected 2 arguments, {} given", args.len() - 1)),
        }
    }
}

pub fn run(cfg: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(cfg.filename)?;
    let result = if cfg.case_sensitive {
        search(cfg.query, &contents)
    } else {
        search_case_insensitive(cfg.query, &contents)
    };

    println!("{}", result.join("\n"));

    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents
        .lines()
        .filter(|line| line.contains(query))
        .collect()
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents
        .lines()
        .filter(|line| line.to_lowercase().contains(&query.to_lowercase()))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_arguments() -> Result<(), String> {
        let args = [
            String::from(""),
            String::from("needle"),
            String::from("haystack.txt"),
        ];

        let cfg = Config::parse(&args)?;

        assert_eq!("needle", cfg.query);
        assert_eq!("haystack.txt", cfg.filename);

        let args = [
            String::from("what-ever"),
            String::from("something"),
            String::from("nothing.txt"),
        ];

        let cfg = Config::parse(&args)?;

        assert_eq!("something", cfg.query);
        assert_eq!("nothing.txt", cfg.filename);

        Ok(())
    }

    #[test]
    fn parse_config_handle_insufficent_arguments() {
        let args = [];
        let result = Config::parse(&args);

        assert_eq!(Err(String::from("Argument list is empty")), result);

        let args = [String::from("")];
        let result = Config::parse(&args);

        assert_eq!(Err(String::from("Expected 2 arguments, 0 given")), result);

        let args = [String::from(""), String::from("")];
        let result = Config::parse(&args);

        assert_eq!(Err(String::from("Expected 2 arguments, 1 given")), result);
    }

    #[test]
    fn parse_config_handle_too_many_arguments() {
        let args = [
            String::from("a"),
            String::from("b"),
            String::from("c"),
            String::from("d"),
        ];
        let result = Config::parse(&args);

        assert_eq!(
            Ok(Config {
                query: "b",
                filename: "c",
                case_sensitive: false
            }),
            result
        );
    }

    #[test]
    fn one_case_sensitive_matching_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));

        let query = "banish";
        let contents = "\
Then there’s a pair of us — don’t tell!
They’d banish us, you know.

How dreary to be somebody!
How public, like a frog";

        assert_eq!(
            vec!["They’d banish us, you know."],
            search(query, contents)
        );
    }

    #[test]
    fn one_case_insensitive_matching_result() {
        let query = "dUcT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(
            vec!["safe, fast, productive.", "Duct tape."],
            search_case_insensitive(query, contents)
        );

        let query = "baNIsh";
        let contents = "\
Then there’s a pair of us — don’t tell!
They’d banish us, you know.

How dreary to be somebody!
How public, like a frog";

        assert_eq!(
            vec!["They’d banish us, you know."],
            search_case_insensitive(query, contents)
        );
    }

    #[test]
    fn multiple_case_sensitive_matching_results() {
        let query = "go";
        let contents = "\
Let us go then, you and I,

When the evening is spread out against the sky

Like a patient etherized upon a table;

Let us go, through certain half-deserted streets,

The muttering retreats";

        assert_eq!(
            vec![
                "Let us go then, you and I,",
                "Let us go, through certain half-deserted streets,"
            ],
            search(query, contents)
        );
    }

    #[test]
    fn multiple_case_insensitive_matching_results() {
        let query = "gO";
        let contents = "\
Let us go then, you and I,

When the evening is spread out against the sky

Like a patient etherized upon a table;

Let us go, through certain half-deserted streets,

The muttering retreats";

        assert_eq!(
            vec![
                "Let us go then, you and I,",
                "Let us go, through certain half-deserted streets,"
            ],
            search_case_insensitive(query, contents)
        );
    }
}
