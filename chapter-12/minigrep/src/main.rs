extern crate minigrep;

use minigrep::Config;
use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    let cfg = Config::parse(&args).unwrap_or_else(|msg| {
        eprintln!("Unable to parse arguments: {}", msg);

        process::exit(1);
    });

    if let Err(e) = minigrep::run(cfg) {
        eprintln!("Application error: {}", e);

        process::exit(1);
    }
}
