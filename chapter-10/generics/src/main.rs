#[derive(Debug)]
struct Point<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point<T, U> {
    fn x(&self) -> &T {
        &self.x
    }

    fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
}

impl Point<f32, f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if largest < item {
            largest = item;
        }
    }

    largest
}

fn main() {
    let numbers = vec![3, 2, 1, 5, 4, 7, 6];

    let largest_number = largest(&numbers);

    println!("The largest number is {}", largest_number);

    let chars = vec!['y', 'm', 'a', 'q'];

    let largest_char = largest(&chars);

    println!("The largest char is {}", largest_char);

    let int_pt = Point { x: 5, y: 10 };
    let float_pt = Point { x: 1.0, y: 4.0 };
    let mixed_type_pt = Point { x: 5, y: 4.0 };
    let string_pt = Point {
        x: "The Spot!",
        y: 'X',
    };

    println!("{:?}, X = {}", int_pt, int_pt.x());
    println!("{:?}, X = {}", float_pt, float_pt.x());
    println!("{:?}, X = {}", mixed_type_pt, mixed_type_pt.x());
    println!("{:?}, X = {}", string_pt, string_pt.x());

    // NOTE: `distance_from_origin` method only available for `Point<f32, f32>`
    // type.
    println!("Distance = {}", float_pt.distance_from_origin());
    // println!("Distance = {}", int_pt.distance_from_origin());
    // println!("Distance = {}", mixed_type_pt.distance_from_origin());
    // println!("Distance = {}", string_pt.distance_from_origin());

    let mixup_pt = int_pt.mixup(string_pt);
    println!("{:?}, X = {}", mixup_pt, mixup_pt.x());
}
