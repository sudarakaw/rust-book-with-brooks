fn main() {
    let mut str1 = "1234567890";
    str1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let str2 = str1;

    println!("{}", str1);
    println!("{}", str2);

    let mut s1 = String::from("0987654321");
    s1 = String::from("abcdefghijklmnopqrstuvwxyz");
    // After you do either of the following, the later use of `s1` in `println`
    // will be an error.

    // let s2 = s1;
    // print_string(s1);

    // This works, but maybe expensive
    // let s2 = s1.clone();
    // print_string(s1.clone());

    // println!("{} {}", s1, s2);

    let s2 = take_and_give_back(s1);
    println!("{}", s2);

    let l = get_length(&s2);
    println!("{} is {} characters long", s2, l);
}

fn print_string(s: String) {
    println!("{}", s);
}

fn take_and_give_back(s: String) -> String {
    println!("Got {}, will give back in a sec.", s);

    s
}

fn get_length(s: &String) -> usize {
    let _ = *s;
    println!("Returning length of {}", s);

    s.len()
}
