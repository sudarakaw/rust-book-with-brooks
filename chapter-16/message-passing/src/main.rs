use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();
    let tx1 = mpsc::Sender::clone(&tx);

    thread::spawn(move || {
        for i in 1..6 {
            tx1.send(format!("{}", i)).unwrap();

            println!("Sent(tx1): {}", i);

            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        for i in 10..16 {
            tx.send(format!("{}", i)).unwrap();

            println!("Sent(tx): {}", i);

            thread::sleep(Duration::from_secs(1));
        }
    });

    // loop {
    //     let received = rx.recv();
    //
    //     match received {
    //         Ok(msg) => println!("Got: {}", msg),
    //         Err(_) => {
    //             break;
    //         }
    //     }
    // }

    for received in rx {
        println!("Got: {}", received);
    }
}
