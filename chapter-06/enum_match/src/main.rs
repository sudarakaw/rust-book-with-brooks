enum USState {
    Colorado,
    NewYork,
    California,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(USState),
}

fn value_in_cents(coin: &Coin) -> u32 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(_) => 25,
    }
}

fn plus_one(opt: Option<i32>) -> Option<i32> {
    match opt {
        None => None,
        Some(value) => Some(value + 1),
    }
}

fn count_coins(coins: Vec<Coin>) -> u32 {
    // NOTE:: `coin` in the closure is of type `&Coin`
    coins.iter().fold(0, |acc, coin| acc + value_in_cents(coin))
}

fn main() {
    println!("Value of Nickel is {}", value_in_cents(&Coin::Nickel));
    println!(
        "Value of California Quarter is {}",
        value_in_cents(&Coin::Quarter(USState::California))
    );

    let five = Some(5);
    let six = plus_one(five);
    let another_six = plus_one(five);
    let none = plus_one(None);

    println!("five: {:?}", five);
    println!("six: {:?}", six);
    println!("another six: {:?}", another_six);
    println!("five (again): {:?}", five);
    println!("none: {:?}", none);

    println!(
        "2 Quarters, 1 Dimes, 4 Pennies & 3 Nickels is: {}",
        count_coins(vec![
            Coin::Quarter(USState::Colorado),
            Coin::Quarter(USState::NewYork),
            Coin::Dime,
            Coin::Penny,
            Coin::Penny,
            Coin::Penny,
            Coin::Penny,
            Coin::Nickel,
            Coin::Nickel,
            Coin::Nickel,
        ])
    );
}
