use std::fs::{self, File};
use std::io::{self, Read};

// Using `match` to explicitly process `Result` returned from `fs` functions.
fn read_user_from_file(file_name: &str) -> Result<String, io::Error> {
    let mut f = match File::open(file_name) {
        Ok(file) => file,
        Err(err) => return Err(err),
    };

    let mut content = String::new();

    match f.read_to_string(&mut content) {
        Ok(_) => Ok(content),
        Err(err) => Err(err),
    }
}

// Using `?` to implicitly handle `Result` returned from `fs` functions.
fn read_user_from_file2(file_name: &str) -> Result<String, io::Error> {
    let mut f = File::open(file_name)?;
    let mut content = String::new();

    f.read_to_string(&mut content)?;

    Ok(content)
}

// Using `?` and method chaining on value contained in `Ok`.
fn read_user_from_file3(file_name: &str) -> Result<String, io::Error> {
    let mut content = String::new();

    File::open(file_name)?.read_to_string(&mut content)?;

    Ok(content)
}

fn read_user_from_file4(file_name: &str) -> Result<String, io::Error> {
    fs::read_to_string(file_name)
}

fn main() {
    match read_user_from_file("/root/bash_history") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }

    match read_user_from_file("/etc/passwd") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }

    match read_user_from_file2("/root/bash_history") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }

    match read_user_from_file2("/etc/passwd") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }

    match read_user_from_file3("/root/bash_history") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }

    match read_user_from_file3("/etc/passwd") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }

    match read_user_from_file4("/root/bash_history") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }

    match read_user_from_file4("/etc/passwd") {
        Ok(content) => println!("{}", content),
        Err(err) => println!("{:?}", err),
    }
}
