use std::fs::File;

fn main() {
    let f = File::open("/tmp/test.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => {
            panic!("Unable to open file {:?}", error);
        }
    };

    println!("{:?}", f);
}
