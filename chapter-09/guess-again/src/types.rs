pub struct Guess {
    value: u8,
}

impl Guess {
    pub fn new(value: u8) -> Result<Guess, String> {
        if 1 > value || 100 < value {
            return Err(format!(
                "Guess value must be between 1 and 100, got {}.",
                value,
            ));
        }

        Ok(Guess { value })
    }

    pub fn value(&self) -> u8 {
        self.value
    }
}
