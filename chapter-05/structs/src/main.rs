// Following is a "Tuple Struct", a struct without field names
#[derive(Debug)]
struct Counts(u32, u32, u32);

fn main() {
    let randoms = Counts(23, 5, 51);
    let plus_one = add_one(randoms);

    println!("{:?}", plus_one);
}

fn add_one(mut t: Counts) -> Counts {
    t.0 += 1;
    t.1 += 1;
    t.2 += 1;

    t
}
