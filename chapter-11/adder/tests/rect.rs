extern crate adder;

mod common;

#[test]
fn shape_overlap() {
    common::setup();

    let larger = adder::Rectangle {
        length: 8,
        width: 7,
    };
    let smaller = adder::Rectangle {
        length: 5,
        width: 1,
    };

    assert!(larger.can_hold(&smaller));
}
