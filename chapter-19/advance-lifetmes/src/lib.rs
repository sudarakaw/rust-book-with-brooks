pub struct Context<'s>(&'s str);

pub struct Parser<'c, 's> {
    ctx: &'c Context<'s>,
}

// NOTE: above worked for me, but according to the book we need to use lifetime
// subtype as follows
// struct Parser<'c, 's: 'c> {
//     context: &'c Context<'s>,
// }

impl<'c, 's> Parser<'c, 's> {
    pub fn parse(&self) -> Result<(), &'s str> {
        Err(&self.ctx.0[1..])
    }
}

pub fn parse_context(context: Context) -> Result<(), &str> {
    Parser { ctx: &context }.parse()
}
